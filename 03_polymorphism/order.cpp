#include <iostream>

struct Parent {
    Parent() {
        std::cout << "try to call method from Parent constructor" << std::endl;
        method();
    }
    ~Parent() {
        std::cout << "try to call method from Parent destructor" << std::endl;
        method();
    }
    virtual void method() {
        std::cout << "Parent method was called" << std::endl;
    }
};
struct Child : public Parent {
    Child() {
        std::cout << "try to call method from Child constructor" << std::endl;
        method();
    }
    ~Child() {
        std::cout << "try to call method from Child destructor" << std::endl;
        method();
    }
    void method() {
        std::cout << "Child method was called" << std::endl;
    }
};

int main() {
    Child child;
    return 0;
}