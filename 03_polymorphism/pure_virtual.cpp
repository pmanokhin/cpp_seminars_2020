#include <iostream>

struct Parent;
void call(Parent& parent);

struct Parent {
    Parent() {
        std::cout << "try to call method from Parent constructor" << std::endl;
        call(*this);
    }
    ~Parent() {
        std::cout << "try to call method from Parent destructor" << std::endl;
        call(*this);
    }
    virtual void method() = 0;
};
struct Child : public Parent {
    Child() {
        std::cout << "try to call method from Child constructor" << std::endl;
        call(*this);
    }
    ~Child() {
        std::cout << "try to call method from Child destructor" << std::endl;
        call(*this);
    }
    void method() {
        std::cout << "Child method was called" << std::endl;
    }
};

void call(Parent& parent) {
    parent.method();
}

int main() {
    Child child;
    return 0;
}