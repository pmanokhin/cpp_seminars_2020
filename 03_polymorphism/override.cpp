#include <iostream>

#include <iostream>

struct Parent {
    virtual void method(unsigned int a) {
        std::cout << "Parent method was called" << std::endl;
    }
};
struct Child : public Parent {
    void method(unsigned int a) override {
        std::cout << "Child method was called" << std::endl;
    }
};

int main() {
    Child child;
    Parent& ref = child;
    ref.method(2);
    return 0;
}