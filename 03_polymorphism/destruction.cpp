#include <iostream>

struct Parent {
    Parent() {
        std::cout << "Parent was created" << std::endl;
    }
    virtual ~Parent() {
        std::cout << "Parent was destroyed" << std::endl;
    }
};
struct Child : public Parent {
    Child() {
        std::cout << "Child was created" << std::endl;
    }
    ~Child() {
        std::cout << "Child was destroyed" << std::endl;
    }
};

int main() {
    // Child child;
    Parent* ptr = new Child();
    delete ptr;
    return 0;
}