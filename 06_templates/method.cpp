#include <iostream>

enum class Color {
    Red,
    Blue,
    Green,
};

enum class Shape {
    Triangle,
    Square,
    Circle
};

class Foo {
public:
    template <class T>
    Foo(T _index) : index(static_cast<int>(_index)) {
        std::cout << "template Foo::Foo()" << std::endl;
    }

    template <class T>
    void set(T val) {
        std::cout << "template Foo::set()" << std::endl;
        index = static_cast<int>(val);
    }

    int index;
};

template <>
Foo::Foo(int _index) : index(_index) {
    std::cout << "Foo::Foo(int)" << std::endl;
}

template <>
void Foo::set<int>(int val) {
    std::cout << "Foo::set(int)" << std::endl;
    index = val ;
}

int main() {
    Foo f1(Color::Blue);
    Foo f2(Shape::Square);
    Foo f3(2);

    f1.set(Color::Blue);
    f2.set(Shape::Square);
    f3.set(2);

    return 0;
}