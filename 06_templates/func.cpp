#include <iostream>

int minimum(int lv, int rv) {
    std::cout << "int minimum" << std::endl;
    return lv < rv ? lv : rv;
}

template <class T>
T minimum(T lv, T rv) {
    std::cout << "template minimum" << std::endl;
    return lv < rv ? lv : rv;
}

int main() {
    minimum(1, 1);
    minimum(1.0f, 1.0f);
    minimum(1, 1.0f);

    return 0;
}