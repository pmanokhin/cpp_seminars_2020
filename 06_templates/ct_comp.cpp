#include <iostream>

template <size_t n>
struct Fact {
    static const size_t result = n * Fact<n - 1>::result;
};
template <>
struct Fact<0> {
    static const size_t result = 1;
};

int main() {
    std::cout << "Fact<6>::result : " << Fact<6>::result << std::endl;
    return 0;
}