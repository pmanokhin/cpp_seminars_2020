#include <iostream>
#include <ctime>

int stack() {
    const size_t size = 10;
    int array[size];
    int result = 0;
    for (size_t i= 0; i < size; i++) {
        result += array[i];
    }
    return result;
}

int dynamic() {
    const size_t size = 10;
    int* array = static_cast<int*>(malloc(size * sizeof(int)));
    int result = 0;
    for (size_t i= 0; i < size; i++) {
        result += array[i];
    }
    free(array);
    return result;
}

void meausre(int (*func)(), size_t sampleCount) {
    std::clock_t begin = clock();
    int result = 0;
    for (size_t i = 0; i < sampleCount; i++) {
        result += func();
    }
    std::cout << clock() - begin << ", " << result << std::endl;
}

int main() {
    meausre(stack, 1024 * 1024);
    meausre(dynamic, 1024 * 1024);
    return 0;
}