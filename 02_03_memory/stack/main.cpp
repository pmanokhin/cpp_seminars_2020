#include <iostream>

void func(int depth) {
    if (depth > 0) {
        int a = 1;
        std::cout << "address of a: " << &a << std::endl;
        func(depth - 1);
    }
}

int main() {
    func(3);
    return 0;
}