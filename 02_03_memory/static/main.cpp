#include <iostream>

struct Trace {
    Trace() { std::cout << "created" << std::endl; }
    ~Trace() { std::cout << "destroyed" << std::endl; }
};

Trace trace;

int main() {
    std::cout << "main" << std::endl;
    return 0;
}