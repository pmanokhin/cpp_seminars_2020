#include <iostream>
#include <string>

struct Trace {
    Trace(const std::string& _name) : name(_name) {
        std::cout << "tracer " << name << " was created" << std::endl;
    }
    ~Trace() {
        std::cout << "tracer " << name << " was destroyed" << std::endl;
    }

    std::string name;
};

class Mother {
public:
    Mother(const std::string& arg) : varA("varA") {
        std::cout << "Mother was created, arg: " << arg << std::endl;
    }
    ~Mother() {
        std::cout << "Mother was destroyed" << std::endl;
    }

    void method() {
        std::cout << "Mother method was called" << std::endl;
    }
private:
    Trace varA;
};

class Father {
public:
    Father(const std::string& arg) : varB("varB") {
        std::cout << "Father was created, arg: " << arg << std::endl;
    }
    ~Father() {
        std::cout << "Father was destroyed" << std::endl;
    }

    void method() {
        std::cout << "Father method was called" << std::endl;
    }
private:
    Trace varB;
};

class Child : public Mother, public Father {
public:
    Child() : 
        Mother("I am your mother"), 
        Father("I am your father"), 
        varC("varC") 
    {
        std::cout << "Child was created" << std::endl;
    }
    ~Child() {
        std::cout << "Child was destroyed" << std::endl;
    }

    void method() {
        Mother::method();
        Father::method();
    }
private:
    Trace varC;
};

int main() {
    Child child;
    child.method();
    child.Mother::method();
    return 0;
}