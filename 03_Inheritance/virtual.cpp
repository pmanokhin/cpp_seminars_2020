#include <iostream>

struct Base {
    Base(int _a) : a(_a) {
        std::cout << "Base was created, this: " << this << ", a: " << a << std::endl;
    }

    int a = 0;
};

struct Mother : public virtual Base {
    Mother(int _a) : Base(_a + 1), a(_a) {
        std::cout << "Mother was created, a: " << a << std::endl;
    }

    int a = 0;
};

struct Father : public virtual Base {
    Father(int _a) : Base(_a + 2), a(_a) {
        std::cout << "Father was created, a: " << a << std::endl;
    }

    int a = 0;
};

struct Child : public Mother, public Father {
    Child() : Base(1), Mother(2), Father(3) {
        std::cout << "Child was created" << std::endl;
    }

    int a = 0;
};

int main() {
    // Mother mother(1);
    // Father father(2);
    Child child;
    return 0;
}