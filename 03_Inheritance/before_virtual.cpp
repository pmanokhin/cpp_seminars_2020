#include <iostream>

struct Base {
    Base() {
        std::cout << "Base was created, this : " << this << std::endl;
    }

    int a = 1;
};

struct Mother : public Base {
    Mother() {
        std::cout << "Mother was created" << std::endl;
    }

    int a = 2;
};

struct Father : public Base {
    Father() {
        std::cout << "Father was created" << std::endl;
    }

    int a = 3;
};

struct Child : public Mother, public Father {
    Child() {
        std::cout << "Child was created" << std::endl;
    }

    int a = 4;
};

int main() {
    Child child;
    std::cout << child.Mother::Base::a << std::endl;
    return 0;
}