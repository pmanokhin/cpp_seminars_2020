#include <iostream>

struct CharChar {
    char a;
    char b;
};

struct ShortShort {
    short a;
    short b;
};

struct InitInt {
    int a;
    int b;
};

struct CharInt {
    char b;
    int a;
};

struct IntChar {
    int a;
    char b;
};


#pragma pack(push, 1)
struct CharIntCharInt {
    char a;
    int b;
    char c;
    int d;
};
#pragma pack(pop)

struct CharCharIntInt {
    char a;
    char b;
    int c;
    int d;
};

int main() {
    std::cout << "sizeof(CharChar): " << sizeof(CharChar) << std::endl;
    std::cout << "sizeof(ShortShort): " << sizeof(ShortShort) << std::endl;
    std::cout << "sizeof(InitInt): " << sizeof(InitInt) << std::endl;
    std::cout << "sizeof(CharInt): " << sizeof(CharInt) << std::endl;
    std::cout << "sizeof(IntChar): " << sizeof(IntChar) << std::endl;
    
    std::cout << "sizeof(CharIntCharInt): " << sizeof(CharIntCharInt) << std::endl;
    std::cout << "sizeof(CharCharIntInt): " << sizeof(CharCharIntInt) << std::endl;

    return 0;
}