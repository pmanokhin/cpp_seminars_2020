#include <iostream>
#include <string>

struct Trace {
    Trace(const std::string& _name) : name(_name) {
        std::cout << "tracer " << name << " was created" << std::endl;
    }
    ~Trace() {
        std::cout << "tracer " << name << " was destroyed" << std::endl;
    }

    std::string name;
};

class Parent {
public:
    Parent(const std::string& arg) : varA("varA") {
        std::cout << "Parent was created, arg : " << arg << std::endl;
    }
    ~Parent() {
        std::cout << "Parent was destroyed" << std::endl;
    }

    void publicMethod() {
        std::cout << "Parent publicMethod was called" << std::endl;

        protectedMethod();
        privateMethod();
    }
protected:
    void protectedMethod() {
        std::cout << "Parent protectedMethod was called" << std::endl;
    }
private:
    void privateMethod() {
        std::cout << "Parent privateMethod was called" << std::endl;
    }

    Trace varA;
};

class Child : public Parent {
public:
    Child() : Parent("I am your father"), varB("varB") {
        std::cout << "Child was created" << std::endl;
    }
    ~Child() {
        std::cout << "Child was destroyed" << std::endl;
    }

    void newPublicMethod() {
        std::cout << "Child newPublicMethod was called" << std::endl;

        protectedMethod();
        // privateMethod(); // error!
    }
private:
    Trace varB;
};

int main() {
    Child child;
    // child.publicMethod();
    // child.newPublicMethod();
    // child.protectedMethod(); // error!
    // child.privateMethod(); // error!
    return 0;
}