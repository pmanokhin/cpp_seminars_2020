#include <iostream>
#include <tuple>

struct Foo {
    template <size_t i> auto get() const;

    int x;
    float y;
};

template <> 
auto Foo::get<0>() const {
    return x;
}
template <> 
auto Foo::get<1>() const {
    return y;
}

int main() {
    Foo foo{2, 2.5f};
    auto&& [x1, y1] = foo;
    const auto& [x2, y2] = foo;
    auto [x3, y3] = foo;

    int x;
    float y;
    auto&& [x4, y4] = std::tuple<int&&, float&&>(std::move(x), std::move(y));

    return 0;
}