#include <iostream>
#include <type_traits>

template <class T>
struct Foo {
    T a;
    T b;
};

Foo<int> pass(int arg) {
    return Foo<int>{arg * arg, arg * arg * arg};
}
Foo<float> pass(float arg) {
    return Foo<float>{arg * arg, arg * arg * arg};
};

template <class T>
auto func(T&& arg) -> decltype(pass(std::forward<T>(arg))) {
    return pass(std::forward<T>(arg));
}

// template <class T>
// auto func(T&& arg) {
//     return pass(std::forward<T>(arg));
// }

template <class T>
auto forwarding(T&& t) {
    return std::forward<T>(t);
}

int main() {
    std::cout << func(2).b << std::endl;
    std::cout << func(2.5f).b << std::endl;

    auto f = forwarding(Foo<int>{1, 2.5f});
    return 0;
}