#include <iostream>

struct Foo {
    // Foo(int _x, int _y) : x(_x), y(_y) {
    //     std::cout << "Foo::Foo(" << _x << ", " << _y << ")" << std::endl;
    // }

    int x;
    int y;
};

int main() {
    Foo foo[2] = {
        {1, 2},
        {10, 20},

        // 1, 2,
        // 10, 20,
    };

    for (size_t i = 0; i < 2; i++) {
        std::cout << foo[i].x << " " << foo[i].y << std::endl;
    }

    return 0;
}