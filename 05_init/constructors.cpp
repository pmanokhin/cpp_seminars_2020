#include <iostream>

struct Foo {
    Foo(int _val) : val(_val) {
        std::cout << "Foo::Foo(" << _val << ")" << std::endl;
    }
    Foo(const Foo& other) : val(other.val) {
        std::cout << "Foo::Foo(Foo(" << other.val << "))" << std::endl;
    }
    Foo& operator = (const Foo& other) {
        val = other.val;
        std::cout << "Foo::operator=(Foo(" << other.val << "))" << std::endl;
        return *this;
    }
    int val;
};

struct Bar {
    // Bar(int a) : x(a), y(a) {}
    // Bar(int _x, int _y) : x(_x), y(_y) {}
    // Bar(Bar&& other) = default;
    // Bar(Bar&& other) : x(std::move(other.x)), y(std::move(other.y)) {}
    // Bar(const Bar&) = default;
    // Bar& operator = (const Bar&) = default;

    Foo x;
    Foo y;
};

void func(const Bar& bar) {
    std::cout << "func(" << bar.x.val << ", " << bar.y.val << ")" << std::endl;
}

int main() {
    // Bar a(1);
    // Bar a {1};
    // Bar a(1, 2);
    Bar a {1, 2};
    Bar b(a);
    a = b;

    // func(Bar(1, 2));

    return 0;
}