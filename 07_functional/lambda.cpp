#include <iostream>
#include <functional>

int main() {
    int a = 2;

    auto add = [a](int b) -> int { return a + b; };
    std::cout << "add(3): " << add(3) << std::endl;
    auto mul = [a](int b) { return a * b; };
    std::cout << "mul(3) : " << mul(3) << std::endl;

    std::function<int(int)> func = add;
    std::cout << "func(3): " << func(3) << std::endl;
    func = mul;
    std::cout << "func(3) : " << func(3) << std::endl;

    return 0;
}