#include <iostream>
#include <functional>

struct Foo {
    int add(int b) {
        return a + b;
    }
    int mul(int b) {
        return a * b;
    }

    int a;
};

using UnInt = int (Foo::*)(int);
// typedef int (Foo::*UnInt)(int);

int main() {
    UnInt mptr = &Foo::add;
    Foo foo = { 2 };
    std::cout << "mptr(foo, 3) : " << (foo.*mptr)(3) << std::endl;
    mptr = &Foo::mul;
    std::cout << "mptr(foo, 3) : " << (foo.*mptr)(3) << std::endl;

    std::function<int (Foo&, int)> func = &Foo::add;
    std::cout << "func(foo, 3) : " << func(foo, 3) << std::endl;
    func = &Foo::mul;
    std::cout << "func(foo, 3) : " << func(foo, 3) << std::endl;

    return 0;
}
