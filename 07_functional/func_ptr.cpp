#include <iostream>
#include <functional>

int add(int a, int b) {
    return a + b;
}
int mul(int a, int b) {
    return a * b;
}

using BinInt = int (*)(int, int);
// typedef int (*BinInt)(int, int);

int main() {
    BinInt fPtr = add;
    std::cout << "fPtr(2, 3): " << fPtr(2, 3) << std::endl;
    fPtr = mul;
    std::cout << "fPtr(2, 3): " << fPtr(2, 3) << std::endl;

    std::function<int(int, int)> f = add;
    std::cout << "f(2, 3): " << f(2, 3) << std::endl;
    f = mul;
    std::cout << "f(2, 3): " << f(2, 3) << std::endl;
    return 0;
}