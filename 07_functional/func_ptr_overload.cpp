#include <iostream>

int callInt(int (*func)(int), int x ) {
    return func(x);
}
int callInt(int (*func)(int, int), int x) {
    return func(x, x);
}

int sub(int a) {
    return -a;
}

int sub(int a, int b) {
    return a - b;
}

int main() {
    // std::cout << "callInt(sub, 10): " << callInt(sub, 10) << std::endl;
    std::cout
        << "callInt( sub, 10 ): "
        << callInt(static_cast<int(*)(int)>(sub), 10) << std::endl;
    return 0;
}