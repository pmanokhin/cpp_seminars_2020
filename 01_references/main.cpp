#include <iostream>
#include <stdint.h>

void basicSyntax() {
    uint64_t a = 10;
    uint64_t& ref = a;

    std::cout << "a: " << a << std::endl;
    std::cout << "ref: " << ref << std::endl;
    std::cout << "sizeof(ref): " << sizeof(ref) << std::endl;

    std::cout << "change a" << std::endl;
    a = 20;
    std::cout << "a: " << a << std::endl;
    std::cout << "ref: " << ref << std::endl;

    std::cout << "change ref" << std::endl;
    ref = 30;
    std::cout << "a: " << a << std::endl;
    std::cout << "ref: " << ref << std::endl;
}

void getAddress() {
    int var = 10;
    int* ptr = &var;
    int& ref = *ptr;

    std::cout << "ptr: " << ptr << std::endl;
    std::cout << "&ref: " << &ref << std::endl;
    std::cout << "ptr == &ref: " << (ptr == &ref) << std::endl;
}

struct FatData {
    int data[ 256 ];
};

int passFatByVal( FatData d, int index ) {
    return d.data[ index ];
}
int passFatByRef( const FatData& d, int index ) {
    return d.data[ index ];
}

void calcSum() {
    FatData d;
    for ( int i = 0; i < 256; i++ ) {
        d.data[ i ] = i;
    }

    int sum = 0;
    for ( int i = 0; i < 16 * 1024; i++ ) {
        for ( int j = 0; j < 256; j++ ) {
            // sum += passFatByVal( d, j );
            sum += passFatByRef( d, j );
        }
    }

    std::cout << "sum: " << sum << std::endl;
}

void passValue( int arg ) {
    std::cout << "passValue, arg: " << arg << std::endl;
    arg++;
    std::cout << "passValue, arg: " << arg << std::endl;
}
void passConst( const int& arg ) {
    std::cout << "passConst, arg: " << arg << std::endl;
    // arg++; // compile error
}
void passMut( int& arg ) {
    std::cout << "passMut, arg: " << arg << std::endl;
    arg++;
    std::cout << "passMut, arg: " << arg << std::endl;
}

void pass() {
    int a = 1;
    std::cout << "before passValue, a: " << a << std::endl;
    passValue( a );
    std::cout << "after passValue, a: " << a << std::endl;

    std::cout << "before passConst, a: " << a << std::endl;
    passConst( a );
    std::cout << "after passConst, a: " << a << std::endl;

    std::cout << "before passMut, a: " << a << std::endl;
    passMut( a );
    std::cout << "after passMut, a: " << a << std::endl;
}

void mut() {
    int a = 1;
    int& mutRef = a;

    const int b = 2;
    const int& constRef = b;
    const int& consRefToMut = a;

    // int& mutRefToConst = b; // complie error
}


int main( int argcCount, char** argCount ) {
    pass();
    return 0;
}