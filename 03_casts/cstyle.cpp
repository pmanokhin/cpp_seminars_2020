#include <iostream>

struct Foo {
    Foo(int _a) : a(_a) {}
    Foo(const Foo&) = default;

    int a = 0;
};

struct Parent {

};
struct Child : public Parent {

};

struct Bar {

};

const char* literal = "literal";

int main() {
    Foo foo = (Foo)1;

    Child child;
    Parent* parentPtr = &child;
    Child* childPtr = (Child*)parentPtr;

    Bar* bar = (Bar*)&foo;

    char* nonConst = (char*)literal;

    return 0;
}