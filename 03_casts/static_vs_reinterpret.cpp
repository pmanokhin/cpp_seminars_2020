#include <iostream>

struct Foo {
    Foo(int _x) : x(_x) {}
    int x = 0;
};
struct Bar {
    Bar(int _y) : y(_y) {}
    int y = 0;
};
struct Child : public Foo, public Bar {
    Child(int f, int b) : Foo(f), Bar(b) {}
};

int main() {
    Child child(10, 20);
    Bar* barPtr = &child;
    Child* stPtr = static_cast<Child*>(barPtr);
    Child* rePtr = reinterpret_cast<Child*>(barPtr);

    std::cout << "stPtr->x: " << stPtr->x << std::endl;
    std::cout << "rePtr->x: " << rePtr->x << std::endl;
    std::cout << "stPtr: " << stPtr << std::endl;
    std::cout << "rePtr: " << rePtr << std::endl;

    return 0;
}