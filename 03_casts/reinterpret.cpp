#include <iostream>

struct Foo {

};
struct Bar {

};

int main() {
    float a = 1.0f;
    // int* aPtr = static_cast<int*>(&a); // error
    int* aPtr = reinterpret_cast<int*>(&a);
    // int b = reinterpret_cast<int>(a); // error

    std::cout << *aPtr << std::endl;

    Foo foo;
    // Bar* bar = static_cast<Bar*>(&foo); // error
    Bar* bar = reinterpret_cast<Bar*>(&foo); // error

    // size_t intPtr = static_cast<size_t>(&a); // error
    size_t intPtr = reinterpret_cast<size_t>(&a);

    return 0;
}