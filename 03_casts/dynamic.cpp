#include <iostream>

struct Base {
    ~Base() {}
    // virtual ~Base() {}
};
struct Foo : public Base {
    Foo(int _a) : a(_a) {}
    ~Foo() {}
    int a = 0;
};
struct Bar : public Base {
    Bar(float _a) : a(_a) {}
    ~Bar() {}
    float a = 0.0f;
};

int main() {
    Bar bar(1.5f);
    Base* basePtr = &bar;
    Bar* barPtr = static_cast<Bar*>(basePtr);

    // Foo foo(5);
    // Base* basePtr = &foo;
    // Bar* barPtr = static_cast<Bar*>(basePtr);

    std::cout << "barPtr: " << barPtr << std::endl;
    std::cout << "barPtr->a: " << barPtr->a << std::endl;

    barPtr = dynamic_cast<Bar*>(basePtr);
    std::cout << "barPtr: " << barPtr << std::endl;
    if (barPtr) {
        std::cout << "barPtr->a: " << barPtr->a << std::endl;
    }

    return 0;
}