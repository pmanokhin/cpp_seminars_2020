#include <iostream>

struct Foo {
    explicit Foo(int _a) : a(_a) {}
    // Foo(int _a) : a(_a) {}
    Foo(const Foo&) = default;

    int a = 0;
};

struct Parent {

};
struct Child : public Parent {

};


int main() {
    float a = static_cast<float>(3);
    Foo foo = static_cast<Foo>(5);

    Child child;
    Parent* parentPtr = &child;
    Child* childPtr = static_cast<Child*>(parentPtr);

    return 0;
}