#include <iostream>
#include "lib.h"

extern int var;
void makeNeagtive();

int main() {
    var = -10;
    std::cout << add(5) << std::endl;

    setVar(10);
    makeNeagtive();
    std::cout << add(5) << std::endl;
    return 0;
}