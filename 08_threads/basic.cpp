#include <iostream>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main() {
    std::thread t1([] () {
        for (int i = 0; i < 4; i++) {
            std::cout << "Hello from thread 1" << std::endl;
            std::this_thread::sleep_for(1s);
        }
    });
    std::thread t2([] () {
        for (int i = 0; i < 6; i++) {
            std::cout << "Hello from thread 2" << std::endl;
            std::this_thread::sleep_for(750ms);
        }
    });
    t1.join();
    t2.join();

    return 0;
}