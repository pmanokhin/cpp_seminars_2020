#include <iostream>
#include <thread>
#include <atomic>

constexpr size_t countersNum = 4;
std::atomic<int> counters[countersNum] = {};

int main() {
    auto func = [] () {
        for (size_t i = 0; i < 1000 * 1000; i++) {
            for (size_t j = 0; j < 4; j ++) {
                counters[j].fetch_add(1);
            }
        }
    };
    std::thread t1(func);
    std::thread t2(func);
    t1.join();
    t2.join();

    for (size_t j = 0; j < 4; j ++) {
        std::cout << counters[j] << std::endl;
    }

    return 0;
}