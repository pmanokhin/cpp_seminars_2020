#include <iostream>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

constexpr size_t countersNum = 4;
volatile int counters[countersNum] = {};
volatile bool lockers[countersNum] = {};
volatile size_t waste = 0;

int main() {
    auto func = [] () {
        for (size_t i = 0; i < 1000 * 1000; i++) {
            for (size_t j = 0; j < 4; j ++) {
                while (lockers[j]) {
                    waste++;
                }
                lockers[j] = true;
                // std::this_thread::sleep_for(1ms);
                counters[j] = counters[j] + 1;
                lockers[j] = false;
            }
        }
    };
    std::thread t1(func);
    std::thread t2(func);
    t1.join();
    t2.join();

    for (size_t j = 0; j < 4; j ++) {
        std::cout << counters[j] << std::endl;
    }
    std::cout << waste << std::endl;

    return 0;
}