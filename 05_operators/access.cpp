#include <iostream>

struct Node {
    Node(int _value, Node* _next) : value(_value), next(_next) {}

    int value;
    Node* next;
};

class NodeIterator {
public:
    NodeIterator(Node* _node) : node(_node) {}
    NodeIterator(const NodeIterator& other) : node(other.node) {}

    NodeIterator& operator ++ () {
        node = node->next;
        return *this;
    }
    NodeIterator operator ++ (int) {
        NodeIterator result(node);
        node = node->next;
        return result;
    }

    NodeIterator& operator = (const NodeIterator& other) {
        node = other.node;
        return *this;
    }

    Node& operator * () const {
        return *node;
    }
    Node* operator -> () const {
        return node;
    }

private:
    Node* node;
};

int main() {
    Node* head = new Node(1, new Node(2, new Node(3, nullptr)));

    NodeIterator i1(head);
    int value11 = (*(++i1)).value;
    int value12 = (*i1).value;
    NodeIterator i2(head);
    int value21 = (i2++)->value;
    int value22 = i2->value;

    std::cout << value11 << " " << value12 << std::endl;
    std::cout << value21 << " " << value22 << std::endl;

    while (head) {
        Node* condemned = head;
        head = head->next;
        delete condemned;
    }

    // about []

    return 0;
}