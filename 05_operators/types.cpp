#include <iostream>

struct Node {
    Node(int _value, Node* _next) : value(_value), next(_next) {}

    int value;
    Node* next;
};

class NodeIterator {
public:
    NodeIterator(Node* _node) : node(_node) {}
    NodeIterator(const NodeIterator& other) : node(other.node) {}

    NodeIterator& operator ++ () {
        node = node->next;
        return *this;
    }
    NodeIterator operator ++ (int) {
        NodeIterator result(node);
        node = node->next;
        return result;
    }

    NodeIterator& operator = (const NodeIterator& other) {
        node = other.node;
        return *this;
    }

    Node& operator * () const {
        return *node;
    }
    Node* operator -> () const {
        return node;
    }

    operator bool () const {
        return node != nullptr;
    }

    // operator int () const {
    //     return node->value;
    // }

private:
    Node* node;
};

int main() {
    Node* head = new Node(1, new Node(2, new Node(3, nullptr)));
    for (NodeIterator i(head); i; i++) {
        std::cout << i->value << std::endl;
    }

    // int num = ++NodeIterator(head);
    // std::cout << "num: " << num;

    while (head) {
        Node* condemned = head;
        head = head->next;
        delete condemned;
    }


    return 0;
}