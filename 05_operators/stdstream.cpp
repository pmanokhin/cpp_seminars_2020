#include <iostream>

struct Complex {
    Complex(double _re = 0.0, double _im = 0.0) : re(_re), im(_im) {}
    Complex(const Complex& other) : re(other.re), im(other.im) {}

    Complex& operator = (const Complex& other) {
        re = other.re;
        im = other.im;
        return *this;
    }

    double re;
    double im;
};

std::ostream& operator << (std::ostream& stream, const Complex& c) {
    stream << c.re << " + " << c.im << "i" << std::endl;
    return stream;
}

int main() {
    Complex c(1.0, 2.0);
    std::cout << c << std::endl;
    return 0;
}