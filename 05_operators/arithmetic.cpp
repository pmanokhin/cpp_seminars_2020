#include <iostream>

struct Complex {
    Complex(double _re = 0.0, double _im = 0.0) : re(_re), im(_im) {}
    Complex(const Complex& other) : re(other.re), im(other.im) {}

    Complex& operator = (const Complex& other) {
        re = other.re;
        im = other.im;
        return *this;
    }

    Complex& operator += (const Complex& other) {
        re += other.re;
        im += other.im;
        return *this;
    }

    Complex& operator -= (const Complex& other) {
        re -= other.re;
        im -= other.im;
        return *this;
    }
    
    Complex operator + (const Complex& other) const {
        return Complex(re + other.re, im + other.im);
    }

    double re;
    double im;
};

Complex operator - (const Complex& a, const Complex& b) {
    return Complex(a.re - b.re, a.im - b.im);
}

void print(const Complex& c) {
    std::cout << c.re << ", " << c.im << std::endl;
}

int main() {
    Complex c1 = Complex(1.0, 2.0) + Complex(3.0, 4.0);
    Complex c2 = Complex(1.0, 2.0) - Complex(3.0, 4.0);

    print(c1);
    print(c2);

    c1 += Complex(10.0, 20.0);
    c2 -= Complex(10.0, 20.0);

    std::cout << "c1 += Complex(10.0, 20.0);" << std::endl;
    std::cout << "c2 -= Complex(10.0, 20.0);" << std::endl;

    print(c1);
    print(c2);

    return 0;
}