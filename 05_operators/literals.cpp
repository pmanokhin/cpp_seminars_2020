#include <iostream>

struct Complex {
    Complex(double _re = 0.0, double _im = 0.0) : re(_re), im(_im) {}
    Complex(const Complex& other) : re(other.re), im(other.im) {}

    Complex& operator = (const Complex& other) {
        re = other.re;
        im = other.im;
        return *this;
    }

    Complex& operator += (const Complex& other) {
        re += other.re;
        im += other.im;
        return *this;
    }

    Complex& operator -= (const Complex& other) {
        re -= other.re;
        im -= other.im;
        return *this;
    }
    
    Complex operator + (const Complex& other) const {
        return Complex(re + other.re, im + other.im);
    }

    double re;
    double im;
};

Complex operator - (const Complex& a, const Complex& b) {
    return Complex(a.re - b.re, a.im - b.im);
}

Complex operator + (double re, const Complex& c) {
    return Complex(re + c.re, c.im);
}

std::ostream& operator << (std::ostream& stream, const Complex& c) {
    stream << c.re << " + " << c.im << "i" << std::endl;
    return stream;
}

Complex operator"" _i(long double value) {
    return Complex(0.0, value);
}

int main() {
    Complex c = 1.0 + 2.5_i;
    c += 0.5 + 1.5_i;
    std::cout << c << std::endl;
    return 0;
}