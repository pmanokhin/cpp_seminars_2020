#include <iostream>

struct Complex {
    Complex(double _re = 0.0, double _im = 0.0) : re(_re), im(_im) {}
    Complex(const Complex& other) : re(other.re), im(other.im) {}

    Complex& operator = (const Complex& other) {
        re = other.re;
        im = other.im;
        return *this;
    }

    Complex& operator = (double _re) {
        re = _re;
        im = 0.0;
        return *this;
    }

    double re;
    double im;
};

void print(const Complex& c) {
    std::cout << c.re << ", " << c.im << std::endl;
}

int main() {
    Complex c1(1.0, 1.0);
    Complex c2(2.0, 2.0);

    print(c1);
    print(c2);

    c1 = c2 = Complex(3.0, 3.0);
    std::cout << "c1 = c2 = Complex(3.0, 3.0);" << std::endl;

    print(c1);
    print(c2);

    c1 = c2 = Complex(5.0);
    std::cout << "c1 = c2 = Complex(5.0);" << std::endl;

    print(c1);
    print(c2);

    return 0;
}