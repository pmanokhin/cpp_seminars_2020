#include <iostream>

#include <stdlib.h>

struct Foo {
    Foo(int _val = 0) : val(_val) {}

    int val;

    static void* operator new(size_t size) {
        std::cout << "Foo::new(" << size << ")" << std::endl;
        return malloc(size);
    }
    static void* operator new[](size_t size) {
        std::cout << "Foo::new[](" << size << ")" << std::endl;
        return malloc(size);
    }
    static void operator delete(void* ptr) {
        std::cout << "Foo::delete" << std::endl;
    }
    static void operator delete[](void* ptr) {
        std::cout << "Foo::delete[]" << std::endl;
    }
};

int main() {
    Foo* foo = new Foo(2);
    delete foo;
    Foo* afoo = new Foo[2];
    delete[] afoo;
    return 0;
}