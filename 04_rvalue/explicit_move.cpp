#include <iostream>
#include <vector>

int main() {
    std::vector<int> v1 = {1, 2, 3};
    std::vector<int> v2(std::move(v1));

    std::cout << "v1:" << std::endl;
    for (int x : v1) {
        std::cout << x << " ";
    }
    std::cout << std::endl;

    std::cout << "v2:" << std::endl;
    for (int x : v2) {
        std::cout << x << " ";
    }
    std::cout << std::endl;

    return 0;
}