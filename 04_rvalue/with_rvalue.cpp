#include <iostream>

struct Foo {
public:
    Foo(int _a) : a(_a) {
        std::cout << "Foo::Foo(" << _a << ")" << std::endl;
    }
    Foo(Foo&& other) : a(other.a) {
        std::cout << "Foo::Foo(Foo&&)" << std::endl;
    }
    Foo(const Foo& other) : a(other.a) {
        std::cout << "Foo::Foo(const Foo&)" << std::endl;
    }
    ~Foo() {
        std::cout << "Foo::~Foo()" << std::endl;
    }

    int a;
};

Foo makeFoo() {
    Foo result(5);
    result.a = 10;
    return result;
}

int main() {
    Foo foo = makeFoo();
    return 0;
}