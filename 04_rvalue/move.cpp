#include <iostream>

struct Foo {
public:
    Foo(int _a) : a(_a) {
        std::cout << "Foo::Foo(" << _a << ")" << std::endl;
    }
    Foo(Foo&& other) : a(other.a) {
        std::cout << "Foo::Foo(Foo&&)" << std::endl;
    }
    Foo(const Foo& other) : a(other.a) {
        std::cout << "Foo::Foo(const Foo&)" << std::endl;
    }
    ~Foo() {
        std::cout << "Foo::~Foo()" << std::endl;
    }

    int a;
};

void localVar() {
    Foo&& rref = Foo(3);
    Foo copy = std::move(rref);
}

void funcArg(Foo&& arg) {
    Foo copy(std::move(arg));
}

struct Bar {
    Bar(int _a) : a(_a) {
        std::cout << "Bar::Bar(int)" << std::endl;
    }
    Bar(Bar&& other) : a(std::move(other.a)) {
        std::cout << "Bar::Bar(Bar&&)" << std::endl;
    }

    Foo a;
};

Bar createBar() {
    Bar result(10);
    return result;
}

int main() {
    // localVar();
    // funcArg(Foo(5));
    // Bar b = createBar();
    return 0;
}